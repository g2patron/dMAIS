# dMAIS

This repository contains the code for the paper "Directional modifier adaptation based on input selection for real-time optimization" by Gabriel Patrón and Luis Ricardez-Sandoval.

Each file runs a different scheme deployed in the paper either on the Williams-Otto or the evaporator case study. The scheme deployed are full modifier adaptation (MA), directional MA (dMA), dMA based on input selection (dMAIS), and dMAIS without operating point adjustment (dMAIS--).
